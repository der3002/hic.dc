library(GenomicAlignments)
library(GenomicRanges)
library(BSgenome)
library(data.table)
library(tldutils)
library(rtracklayer)

library(HiC.DC)
args <- commandArgs(trailingOnly = TRUE)
options(warn=-1)

# Taken from http://www.r-bloggers.com/parse-arguments-of-an-r-script/
parseArgs <- function(x) strsplit(sub("^--", "", x), "=")
args.df <- as.data.frame(do.call("rbind", parseArgs(args)))
args.list <- as.list(as.character(args.df$V2))
names(args.list) <- args.df$V1

gen <- args.list$g
gen.ver <- args.list$v
sig <- args.list$s
bin.type <- args.list$bt
if(bin.type == "Bins-RE-sites") numsites <- as.integer(args.list$nm)
if(bin.type == "Bins-uniform") binsize <- as.integer(args.list$bs)
output.path <- args.list$o
wg.file <- args.list$wg

genome <- paste("BSgenome.", gen, ".UCSC.", gen.ver, sep="")
library(genome, character.only = TRUE)
chrom <- setdiff(seqnames(get(gen))[sapply(seqnames(get(gen)), nchar) <= 5], 'chrM')

# Check if chrom is given as an argument.
# If it is entered, converted it to vector:
if(!is.null(args.list$chr)) {
str <- strsplit(args.list$chr, " ")[[1]]
chrom <- as.character(str)
}

cat("Using", chrom, "and cut size of", sig, "\n")

genome.chromSizes <- data.table(chr=chrom, size=seqlengths(get(gen))[chrom])
genome.chromGR <- GRanges(seqnames = genome.chromSizes$chr, ranges = IRanges(start = 1, end = genome.chromSizes$size))
enzymeCuts <- lapply(chrom,function(x){GRanges(seqnames=x, ranges=ranges(matchPattern(sig, get(gen)[[x]])))})
enzymeCuts <- do.call(c, enzymeCuts)
saveRDS(enzymeCuts, file=sprintf('%s_enzymeCuts.rds', output.path))

if(bin.type == "Bins-RE-sites"){

  # We construct human genome based on enzyme cutting sites

  enzymeBins <- setdiff(genome.chromGR, enzymeCuts)
  enzymeBinsTab <- data.table(chr = as.character(seqnames(enzymeBins)),
                              start = as.integer(start(enzymeBins))-2,
                              end = as.integer(end(enzymeBins))+2)
  setkey(genome.chromSizes,chr)
  enzymeBinsTab$chrSize <- genome.chromSizes[enzymeBinsTab$chr]$size
  enzymeBinsTab[start < 1]$start <- enzymeBinsTab[start < 1]$start+2
  enzymeBinsTab[end > chrSize]$end <- enzymeBinsTab[end > chrSize]$end-2
  enzymeBins <- GRanges(seqnames = enzymeBinsTab$chr, IRanges(start = enzymeBinsTab$start, end = enzymeBinsTab$end))
  enzymeBinsTab$name <- sprintf('%s-%s-%s',enzymeBinsTab$chr, enzymeBinsTab$start, enzymeBinsTab$end)
  rm (genome.chromSizes, genome.chromGR, enzymeBins)
  cat("Reseting", "\n")
  gc()
  gc(reset=TRUE)

  # We cluster enzyme cutting sites every 'numsites' consecutive bins

  enzymeBinsGrouping <- list()
  for(i in chrom){
    chrEnzymeBins <- enzymeBinsTab[chr == i][order(start)]
    binNumbers <- 1:ceiling(nrow(chrEnzymeBins)/numsites)
    chrEnzymeBins$binNumb <- rep(binNumbers, each=numsites)[1:nrow(chrEnzymeBins)]
    chrEnzymeBins$binName <- sprintf('%s-%s', i, chrEnzymeBins$binNumb)
    enzymeBinsGrouping[[i]] <- chrEnzymeBins
  }
  enzymeBinsTab <- do.call(rbind, enzymeBinsGrouping)
  binsTab <- enzymeBinsTab[,list(chr=chr[1], start=min(start), end=max(end)), by=binName]
  binsTab$name <- sprintf('%s-%s-%s', binsTab$chr, binsTab$start, binsTab$end)
  binsGR <- GRanges(seqnames = binsTab$chr, IRanges(start=binsTab$start, end=binsTab$end))
  gc <- unlist(sapply(chrom, function(x){GC(chr = x, regions = binsGR[which(seqnames(binsGR)==x)], genome=gen, version=gen.ver)}))
  map <- unlist(sapply(chrom, function(x){Mapp(chr = x, con = wg.file , regions = binsGR[which(seqnames(binsGR)==x)], genome = gen, version=gen.ver)}))
  binsGR$gc <- gc
  binsGR$map <- map
  names(binsGR) <- binsTab$name
  rm(chrEnzymeBins,enzymeBinsTab)
  cat("Resetting", "\n")
  gc()
  gc(reset=TRUE)
}

if(bin.type == "Bins-uniform"){

  bins.chrom <- function(chr, binsize){
    cuts <- seq(binsize, seqlengths(get(gen))[chr] - (seqlengths(get(gen))[chr] %% binsize) + binsize, by = binsize)
    bins <- GRanges(chr, IRanges(end = cuts[-length(cuts)], width = binsize))
    return(bins)
  }
  binsGR <- lapply(1:length(chrom), function(x){bins.chrom(chrom[x], binsize)})
  binsGR <- do.call(c, binsGR)
  names(binsGR) <- sprintf('%s-%d-%d', as.character(seqnames(binsGR)), start(binsGR), end(binsGR))

  gc <- unlist(sapply(chrom, function(x){GC(chr = x, regions = binsGR[which(seqnames(binsGR)==x)], genome=gen, version=gen.ver)}))
  len <- unlist(sapply(chrom, function(x){Len(chr = x, regions = binsGR[which(seqnames(binsGR)==x)], pattern=sig, genome=gen, version=gen.ver)}))
  map <- unlist(sapply(chrom, function(x){Mapp(chr = x, con = wg.file , regions = binsGR[which(seqnames(binsGR)==x)], genome=gen, version=gen.ver)}))
  binsGR$len <- len
  binsGR$gc <- gc
  binsGR$map <- map
}
saveRDS(binsGR, sprintf('%s_binsGR.rds', output.path))
