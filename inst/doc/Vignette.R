## ----setup, include=FALSE------------------------------------------------
knitr::opts_chunk$set(echo = TRUE)

## ----message=FALSE, cache=FALSE------------------------------------------

library(HiC.DC)
options(warn=-1)

#find the location of the data folder
path.data=system.file("data", package="HiC.DC")

genome = 'Hsapiens'                                                          # Genome
version = 'hg19'                                                             # Version of build
bamfiles1 = paste0(path.data,"/602_1_chr21_little.bam")
bamfiles2 = paste0(path.data,"/602_2_chr21_little.bam")

         # Hi-C Bam files
chr = 'chr21'                                                                # chromosome
cores = 1                                                                    # Number of threads
outputfile = paste0(path.data,"/602_chr21_little.rds")
         # Out put file

filterHiC(genome, version, bamfiles1, bamfiles2, outputfile, chr, cores)

## ----load reads----------------------------------------------------------

data = readRDS(outputfile)
data

## ----MakeBins------------------------------------------------------------

path = paste0(path.data,'/chr21')
fname = paste0(path.data,'/wgEncodeCrgMapabilityAlign50mer_chr21.bigWig')
chr = 'chr21' 
genome = 'Hsapiens'                                                          
version = 'hg19'  

Make_Bins(genom = genome, genom.version = version, bin.type = 'Bins-uniform', sig = 'GATC', 
          binsize = 50e3, output.path = path, con = fname, chrom = chr)

## ----Runhic--------------------------------------------------------------

read.file = paste0(path.data,'/602_chr21_little.rds')
bin.file = paste0(path.data,'/chr21_binsGR.rds')
enzyme.file = paste0(path.data,'/chr21_enzymeCuts.rds')
output.file = paste0(path.data,'/HiC_Counts_chr21.rds')

runhic(genome, version, readfile = read.file, 
       binfile = bin.file, REfile = enzyme.file, outfile = output.file,
       chrom = chr)

## ----Construct  Features-------------------------------------------------

path = path.data
file.prefix = 'HiC_Counts'
output = paste0(path.data,'/HiC_data')

Make_Features(path, file.prefix, bin.file, chr, 'Bins-uniform', output = output)

## ----HiC-DC--------------------------------------------------------------
file = paste0(path.data,'/HiC_data_Features_80kb_on_chr12.rds')

df = 6
ssize = 0.8
binsize = 8e4
chunksize = 1.4e4
output = paste0(path.data,'/HiC_data')
hic.dc.fixedbin(file = file, df = df, ssize = ssize, cores = cores, binsize = binsize, output = output, chunksize = chunksize)
system(paste0("rm ",path.data,"/HiC_data_Model-Fit_on_chr12.rds"))

## ----HiC-DC results------------------------------------------------------
data = readRDS(paste0(path.data,"/HiC_data_Results_on_chr12.rds"))
signif.contacts = data[qvalue < 0.05 & D!=0, list(binI=binI, binJ=binJ, 
                         counts=counts, qvalue=qvalue)]
head(signif.contacts)

## ----plot, message=FALSE, cache=FALSE, fig.width=8, fig.height=8.5-------

start = 11500000
end = 11840000
size = 1e5
col = c("red","blue","darkgreen" ,"brown")

regions = readRDS(paste0(path.data,'/chr10_binsGR.rds'))
trackslist = readRDS(paste0(path.data,'/trackslist.rds'))
contact.matrix = readRDS(paste0(path.data,'/HiC_data_Results_on_chr10_20kb.rds'))

Signaltracks_hic(contact.matrix = contact.matrix, regions = regions,
                 start=start, end=end, trackslist=trackslist, 
                 size = size, col=col)

## ----Sushi, fig.width=8, fig.height=8------------------------------------
sashimi.plot(contact.matrix = contact.matrix, chromstart = start, 
             chromend = end, regions = regions)

