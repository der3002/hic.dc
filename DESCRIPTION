Package: HiC.DC
Type: Package
Date: 2016-06-16
Title: HiC-DC assigns statistical significance to chromatin interactions from
    Hi-C sequencing data
Version: 0.1.0
Author: Mark Carty and Merve Sahin
Maintainer: Mark Carty <mrk.carty@gmail.com>
Description: Hi-C-DC provides methods to test for DNA looping event by use of
    a hurdle negative binomial generalized linear model. The package also provides
    methods to visualize the data.
License: GPL-2
LazyData: TRUE
Imports:
    VGAM (== 1.0-2),
    pscl,
    doParallel,
    data.table,
    BSgenome,
    Sushi,
    GenomicAlignments,
    rtracklayer,
    GenomicRanges,
    Matrix,
    aqfig,
    ggbio,
    ggplot2,
    scales,
    foreach,
    stringr
Depends:
    R (>= 3.3.1)
Suggests:
    rmarkdown,
    knitr,
    BSgenome.Hsapiens.UCSC.hg19,
    BSgenome.Hsapiens.UCSC.hg38,
    BSgenome.Mmusculus.UCSC.mm10,
    EnsDb.Hsapiens.v75
RoxygenNote: 5.0.1
VignetteBuilder: knitr
