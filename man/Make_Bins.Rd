% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/MakeBins.R
\name{Make_Bins}
\alias{Make_Bins}
\title{Creates a GRanges object of genomic regions that tiles the genome}
\usage{
Make_Bins(genom = "Hsapiens", genom.version = "hg19", sig = "AAGCTT",
  bin.type = NULL, numsites = 10, binsize = 40000, output.path, con)
}
\arguments{
\item{genom}{The reference genome}

\item{sig}{The restriction enzyme signature}

\item{bin.type}{The type of grid}

\item{numsites}{The number of consecutive restriction enzyme sites to be aggregated}

\item{binsize}{The size of the uniform bins in base pairs}

\item{con}{The path to bigWig file of UCSC 50 mer sequences uniquely aligned to specified reference genome}

\item{genom.ver}{The version of reference genome}
}
\value{
GRanges object of genomic regions that partitions the genome, with a numeric value for Hi-C biases
}
\description{
Creates a GRanges object of genomic regions that tiles the genome
}

