#### _Mark A. Carty and Merve Sahin_

#### _2/28/2017_

HiC-DC package provides tools for data processing, estimating the statistical significance of chromatin interactions, and
visualizing chromosomal contact maps produced by Hi-C. HiC-DC tests for chromatin interactions 
by using a zero-inflated negative binomial generalized linear model. 

Please see the Vignette.html in **inst/doc** folder that explains the use of the package and demonstrates typical workflows.

